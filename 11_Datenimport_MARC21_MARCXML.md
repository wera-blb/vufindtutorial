# Datenimport MARC21 MARCXML

In diesem Kapitel nutzen wir exemplarisch Daten der Badischen Landesbibliothek Karlsruhe. Diese stellt das BSZ als sogenannte Open Data zur Verfügung.
<https://wiki.k10plus.de/display/K10PLUS/Open+Data>

## Download

Wir nutzen nur einen Teil der Titeldaten für unsere Tutorial, ein voller Import würde den Rahmen zeitlich sprengen.

Im Terminal im Verzeichnis ~/Downloads folgendes eingeben (**funktioniert nur innerhalb der BLB**)

Sie finden die Datei TA-MARC-018-220613.tar.gz auch im Ordner /media/11/ in Ihrem Verzeichnis, sollte der Download nicht funktionieren
```bash
cd ~/Downloads
wget adis.blb-intranet.de/vufindtutorial/media/11/TA-MARC-018-220613.tar.gz
```
Bei Probleme auch unter ```/home/vboxuser/DocumentsShared/virtualbox/``` zu finden.


![](media/11/image1.png)

## Entpacken

Entpacken Sie die heruntergeladene Datei in das Verzeichnis `/usr/local/vufind/local/harvest/`.
```bash
tar xfvz TA-MARC-018-220613.tar.gz -C /usr/local/vufind/local/harvest/
```
Erstellen Sie im Verzeichnis `/usr/local/vufind/local/harvest/`ein neues Verzeichnis namens `BLB`. Kopieren Sie die Datei `018-tit.mrc` in das Verzeichnis `BLB`.

## Import vorbereiten

Öffnen Sie mit Mousepad die Datei `marc_local.properties` im Verzeichnis `/usr/local/vufind/import/`. Ändern Sie die beim Testimport editierten Werte „collection“ und „institution“ wie folgt:

```
collection = "BLB"
institution = "Badische Landesbibliothek"
```

## Import durchführen

Starten Sie VuFind.

Führen Sie im Terminal die folgenden Befehle aus:

```bash
cd /usr/local/vufind/harvest/
./batch-import-marc.sh BLB
```

Der zweite Befehl startet den Import. Dieser verarbeitet nacheinander die einzelnen MRC-Dateien und meldet den Import jedes Datensatzes im Terminal:

![](media/11/image3.png)

Während des Importes werden die importierten Dateien in ein Verzeichnis
namens `processed` verschoben. Wird der Import nach einer Unterbrechung fortgesetzt, werden do die Dateien im Ordner `processed` nicht nochmals importiert.

Der Import ist zu Ende wenn eine ähnliche Meldung kommt

![](media/11/image4.png)

## VuFind neu starten und Index optimieren

Starten Sie VuFind neu.
```bash
/usr/local/vufind/solr.sh restart
```

Optimieren Sie den Index.
```bash
php /usr/local/vufind/util/optimize.php
```

## Import kontrollieren

Führen Sie eine leere Suche in VuFind durch.
Eine mögliche Ansicht, hier mit Format "E-Book":

![](media/11/image5.png)

Anhand der Facette „Bestand“ können Sie sehen, dass zu 4521 Datensätze aus der BLB Datei hinzugekommen sind.

## Sicherungspunkt

Und jetzt unbedingt einen Sicherungspunkt namens „Datenimport abgeschlossen“ anlegen!

## Quellen

MARC Records. VuFind Documentation.
<https://vufind.org/wiki/indexing:marc>
<https://vufind.org/wiki/importing_records>

Re-Indexin. VuFind Documentation.
<https://vufind.org/wiki/indexing:re-indexing>

BSZ: Allgemeines zur Datenbereitstellung
<https://wiki.k10plus.de/display/K10PLUS/Allgemeines+zur+Datenbereitstellung>

BSZ: ELN und ILN, Verzeichnisstruktur auf dem FTP-Server
<https://wiki.k10plus.de/display/K10PLUS/Allgemeines+zur+Datenbereitstellung#AllgemeineszurDatenbereitstellung-ELNundILN,VerzeichnisstrukturaufdemFTP-Server>

BSZ: Teilnahme am K10plus
<https://wiki.k10plus.de/display/K10PLUS/Teilnahme+am+K10plus>

BSZ: Open Data
<https://wiki.k10plus.de/display/K10PLUS/Open+Data>
