# Installation von Linux

VuFind kann unter der Betriebssystemen Windows und Linux installiert werden. Ursprünglich wurde VuFind für den Betrieb unter Linux entwickelt und die Installation unter Linux ist daher einfacher.

Die unterschiedlichen Versionen von Linux-Betriebssystemen werden Distributionen genannt. Eine gängige Distribution ist Ubuntu. Ubuntu gibt es in verschiedenen Varianten, welche sich vor allem durch die mitgelieferte Software und die Nutzung von Computerressourcen wie Prozessorleistung und Arbeitsspeicher unterscheiden. Eine ressourcenschonende Variante ist Xubuntu. Dieses werden wir in der aktuellen Version Xubuntu 22.04.2 LTS als Virtuelle Maschine in VirtualBox installieren.

## Download der Datei für die Installation

Öffnen Sie in Ihrem Browser die Internetseite von Ubuntu:
<http://ftp.uni-kl.de/pub/linux/ubuntu-dvd/xubuntu/releases/22.04/release/>

Wählen Sie den Eintrag "xubuntu-XX-XX-deskktop-amd64.iso“:

![](media/02/image1.jpeg)


Laden Sie die Installationsdatei auf Ihren Desktop herunter.

## Virtuelle Maschine erzeugen

Starten Sie VirtualBox:

![](media/02/image2.png)

Klicken Sie „Neu“ oder "Maschine->Neu" an.

Es öffnet sich der Dialog zur Erzeugung einer neuen Virtuellen Maschine:

![](media/02/image3.png)

Geben Sie der Virtuellen Maschine einen Namen (hier „VuFind01“). 


Wählen Sie das ISO Abbild aus, die Datei die Sie heruntergeladen haben.

Als Typ wird automatisch „Linux“ und als Version „Ubuntu (64 bit)“ ausgewählt.

Klicken Sie anschließend "Unbeaufsichtigte Installation" an.

Sie gelangen zur weiteren Einrichtung des Systems

![](media/02/image4.png)

Geben Sie einen Namen und ein Passwort ein dass Sie sich bitte merken!

Aktivieren Sie das Häkchen zur Installation der Gasterweiterungen und wählen Sie die Datei ```C:\Program Files\Oracle\VirtualBox\VBoxGuestAdditions.iso``` aus.

Klicken Sie anschließend "Hardware" an.

Die Einstellungen für die Festplatte der Virtuellen Maschine werden abgefragt:

![](media/02/image5.png)

Die Anzeige orientiert sich am Hauptspeicher Ihres Rechners – dieser wird aus Sicht von VirtualBox als Host bezeichnet. Der in der Abbildung gezeigte Rechner hat einen Hauptspeicher von 8 Gigabyte. Der grüne Balken zeigt die maximal empfohlene Größe an.
Wählen Sie einen Wert, der nahe der maximal empfohlenen Größe ist.
Klicken Sie anschließend „Vorwärts“.

Klicken Sie nun auf "Festplatte" und legen Sie den Dateityp der virtuellen Festplatte fest:

![](media/02/image6.png)


Wählen Sie „Jetzt eine virtuelle Festplatte erstellen“ aus.

Stellen Sie als Größe mind. 100GB ein´.

Klicken Sie anschließend „Vorwärts“ an.

Sie Sehen eine Zusammenfassung Ihrer Einstellungen.

Klicken Sie nurn auf "Fertigstellen".

Die Virtuelle Maschine wird erzeugt und startet automatisch, das dauert seine Zeit...

In der Liste der Virtuellen Maschinen wird die VM auch angezeigt:

![](media/02/image9.png)

Starten Sie die Virtuelle Maschine durch Anwahl des grünen Pfeils „Starten“ falls diese nicht automatisch gestartet wurde.

![](media/02/image10.png)


Nach geraumer Zeit erscheint der Schriftzug „Xubuntu" auf schwarzem Hintergrund, die virtuelle Maschine wird geladen.

Loggen Sie sich nun mit Ihren zuvor vergebenen Daten ein.

![](media/02/image13.png)

Es erscheint der Startbildschirm der Installation. 
Die Sprache ist noch auf englisch.
[Hier](https://www.bimminger.at/content/bereich/tips_diverses/tipp_tastaturlayout.html) finden Sie eine Erklärung des Tastatur Layouts.

Um die Sprache auf Deutsch umzustellen müssen Sie erst noch einige Anpassungen durchführen.

Springen Sie nun direkt zu [Durchführung innerhalb der BLB](https://adis.blb-intranet.de/vufindtutorial/#/02_Installation_Xubuntu?id=durchf%c3%bchrung-innerhalb-der-blb)


<!-- 


Klicken Sie „Xubuntu installieren“ an.

Belassen Sie die Tastaturbelegung wie sie ist auf "German" und klicken Sie "Weiter".


Die beiden angebotenen Optionen „Aktualisierungen während der Installation herunterladen“ und „Software von Drittanbietern“ sollten nicht ausgewählt werden. Die Aktualisierungen für das System können später separat durchgeführt werden. Software von Drittanbietern wird für den späteren Betrieb von VuFind nicht benötigt.
Klicken Sie „Weiter“ an.



Im nächsten Dialog wird die Installationsart abgefragt:

![](media/02/image15.png)

Die ausgewählte Option „Festplatte löschen und Xubuntu installieren“ ist optimal. Gemeint ist die Festplatte der Virtuellen Maschine und diese kann für die Installation gefahrlos gelöscht werden.
Klicken Sie „Jetzt installieren“ an (nicht im Bild zu sehen).

Es erscheint ein Popup-Fenster (Angaben können bei Ihnen abweichen):

![](media/02/image16.png)

Klicken Sie „Weiter“ an.

Im nächsten Dialog „Wo befinden Sie sich?“ (ohne Abbildung) stellen Sie die Zeitzone ein. Diese sollte „Berlin“ sein. Klicken Sie anschließend „Weiter“ an.

Der nächste Dialog fragt „Wer sind Sie?“:

![](media/02/image17.png)

Füllen Sie die Felder entsprechend aus:

![](media/02/image18.png)

Die Felder „Name Ihres Rechners“ und „Bitte Benutzernamen auswählen“ werden anhand der Angabe in „Ihr Name“ vorbelegt. Diese Vorbelegung können Sie so belassen. Wenn Sie bei jeder Anmeldung Ihr Passwort eingeben möchten, wählen Sie „Passwort zum Anmelden abfragen“ aus und ansonsten „Automatische Anmeldung“. Der Einfachheit halber wählen Sie "Automatisch anmelden" aus.
Klicken Sie anschließend „Weiter“ an.

***DAS PASSWORT FÜR DAS VM-DUMMY-IMAGE IST "Erbprinz2022"***

Die Installation startet und informiert während des Verlaufs über die aktuell stattfindenden Aktionen. Dies kann durchaus ein paar Minuten dauern...

Das Ende der Installation wird gemeldet:

![](media/02/image19.png)

Klicken Sie „Jetzt neu starten“ an.

Während des Neustarts werden Sie (evtl.) zum Entfernen des Installationsmediums aufgefordert.

Da ein virtuelles Installationsmedium in Form einer ISO-Datei verwendet wurde, ist weder ein zu entfernendes Medium noch eine Schublade vorhanden.
Drücken Sie „Enter“.

Xubuntu startet nun und es erscheint der Desktop. Das Startmenü befindet sich oben links, daneben ist die Taskleiste. Dort sollte bereits nach wenigen Sekunden die „Aktualisierungsverwaltung“ erscheinen.
 -->



## Durchführung innerhalb der BLB
+ Wenn Sie dieses Tutorial innerhalb der BLB durchführen müssen Sie noch die Proxy Konfiguration anpassen, sonst haben Sie keinen Zugriff auf das Internet.
  (Klicken um diesen Abschnitt zu schliessen) +

  Testen Sie vorweg ob Sie in das Internet kommen, rufen Sie über das Menü links oben den Browser (Internetnavigator) auf und rufen Sie eine beliebige Website auf.

  ![](media/02/image21-1.png)

  Wenn Sie die Seite angezeigt bekommen, dann kommen Sie auch ins Internet und müssen nichts weiter tun.

  Wenn nicht, dann ist folgendes nötig:
  * Im Firefox gehen Sie in die Einstellungen, scrollen nach unten bis zu den "Verbindungs-Einstellungen (Connection Settings)" und klicken hier "Einstellungen" an

  ![](media/02/image21-2.png)

  Wählen Sie im sich öffnenden Fenster
  * Manuelle Proxy-Konfiguration
  * unter HTTP-Proxy tragen Sie folgendes ein:
  * 172.20.10.2, Port:8080
  * Aktivieren Sie "Diesen Proxy auch für HTTPS verwenden"

  Klicken Sie auf ok, schließen Sie die Einstellungen und rufen erneut eine  Seite aus dem Internet auf, die Verbindung müsste jetzt klappen.

  Im folgenden müssen Sie einige Befehle als Systemadministrator (root) ausführen.

  Damit dies klappt und einfacher ist, werden wir Ihnen als Nutzer vollen Zugriff erlauben.
  
  Dazu bitte folgendes durchführen:

  ```bash
  su
  ```
  Und nun Ihr Passwort eigeben.
  
  ```bash
  visudo /etc/sudoers
  ```

  In der Datei bis zum Ende scrollen und folgendes eingeben, wobei hier ```username``` mit Ihrem Nutzernamen zu ersetzen ist

  ```bash
  username  ALL=(ALL) NOPASSWD:ALL
  ```



  Um Software zu installieren und zu aktualisieren sind noch weitere Schritte nötig, die bisherigen Einstellungen gelten nur für den Firefox.

  * Rufen Sie das Terminal über das Menü auf

  ![](media/02/image21-3.png)

  * Tippen Sie folgendes ein

  ```bash
  sudo -s
   ```

  
  ```bash
  nano /etc/apt/apt.conf.d/proxy01
  ``` 

  Dann folgendes eingeben:
  ```bash
  Acquire::http::Proxy "http://172.20.10.2:8080";
  Acquire::https::Proxy "https://172.20.10.2:8080";
  Acquire::ftp::Proxy "http://172.20.10.2:8080";
  ```

  Datei mit Strg+X speichern und bejahen.

  In der Konsole nun noch folgendes angeben, da Ubuntu mittlerweile den Snap Store als Software Verteiler nutzt.
  ```bash
  sudo snap set system proxy.http="http://172.20.10.2:8080"
  ```
  Und Enter. Und danach noch für https:
  ```bash
  sudo snap set system proxy.https="http://172.20.10.2:8080"
  ```

  Damit diese Daten persistent bleiben noch folgendes ausführen:
  ```bash
  sudo visudo
  ```
  und hier folgendes einfügen (egal wo)
  ```bash
  Defaults env_keep = "http_proxy https_proxy ftp_proxy"
  ```
  Speichern & beenden (Mit Strg+X)

    Daraufhin
  ```bash
  apt-get update
  ```
  ausführen um das System zu aktualisieren.

  Nun wird auch schon gleich eine Verbindung aufgebaut.


  Klicken Sie links oben auf das Icon, dann wählen Sie rechts "Settings" aus und linken Untermenü daraufhin "Language Support" 

![](media/02/image14.png)

Wählen Sie unter "Install/Remove Langueages..." German aus.

Ziehen Sie danach German an oberste Stelle.

Starten Sie das System neu.

Die Tastaturbelegung müssen Sie auch noch auf Deutsch umstellen.
Hierzu klicken Sie wieder links oben auf das Icon, dann wählen Sie rechts "Einstellugen" aus und linken Untermenü daraufhin "Tastatur" .
Wechseln Sie zum Tab "Tastaturbelegung"
Deaktivieren Sie den Schalter "Systemvorgaben verwenden", fügen Sie Deutsch als Sprache hinzu.

![](media/02/image41.png)

Kommt nach dem Neustart die Frage ob Sie die Ordnernamen aktualisieren/lokalisieren sollen so führen Sie dies durch.

Es kann vorkommen dass die Sprachunterstützung noch aktualisiert werden muss, dies können Sie gerne machen, ist aber nicht relevant.

![](media/02/image21.png)


Klicken Sie „Jetzt installieren“ an.

Die Aktualisierung startet, je nach Größe der zu aktualisierenden Dateien (im Bsp. 424,5 MB) kann dies einige Zeit dauern.

Um Informationen zu den stattfindenden Aktion anzuzeigen, klicken Sie auf „Details“. Nach Abschluss der Aktualisierung muss Xubuntu möglicherweise nochmals neu gestartet werden.

![](media/02/image23.png)

Während der Aktualisierung wird Ihnen auffallen, dass sich die Bildschirmauflösung von Xubuntu nicht vergrößern lässt. Um dieses Problem zu beheben, sollten Sie die sogenannten „Gasterweiterungen“ installieren.

## Gasterweiterungen installieren (Nicht mehr nötig)

Das Fenster, in welchem VirtualBox Ihnen den Desktop von Xubuntu anzeigt, hat ein eigenes Menü mit den Menüpunkten „Maschine“, „Anzeige“, „Geräte“ und „Hilfe“:

![](media/02/image24.png)

Wählen Sie aus dem Menü „Geräte“ den Menüpunkt „Gasterweiterungen einlegen…“ aus:

![](media/02/image25.png)

Dadurch wird virtuell eine CD in das CD-Laufwerk von Xubuntu eingehängt (dieser Vorgang nennt sich „Mounten“). Als Bestätigung für das erfolgreiche Einhängen, wird für kurze Zeit der Text „CD wurde eingehängt“ eingeblendet (ohne Abbildung). Anschließend wird der Inhalt der CD im Dateimanager von Xubuntu angezeigt (ohne Abbildung).
Schließen Sie den Dateimanager.

Das Startmenü von Xubuntu befindet sich in der linken oberen Ecke hinter dem blauen Symbol. Öffnen Sie das Startmenü und wählen Sie die Anwendung „Terminal“ aus:

![](media/02/image26.png)

Das Terminal öffnet eine sogenannte Kommandozeile. Dort können Sie Systembefehle mit der Tastatur eingeben:

![](media/02/image27.png)

Tippen Sie im Terminal diesen Befehl ein und führen diesen durch Drücken der Enter-Taste aus:

```bash
sudo apt install dkms
```

  --------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------
  **!!!**   Achten Sie bei allen Eingaben unbedingt auf die korrekte Einhaltung der Groß- und Kleinschreibung. Im Unterschied zu Windows unterscheidet Linux diese streng.
  --------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------

Nach Ausführung des Befehls erscheint diese Meldung im Terminal:

![](media/02/image28.png)

Geben Sie Ihr Passwort ein – die Eingabe selbst ist nicht sichtbar - und drücken Sie anschließend die Enter-Taste. Dadurch autorisieren Sie die Ausführung des Befehls „sudo“.
Der gesamte Befehl installiert eine notwendige Zusatzkomponente namens „dkms“. Während der Installation müssen Sie die Frage „Möchten Sie fortfahren \[J/n\]?“ (ohne Abbildung) mit Eingabe von „j“ bejahen.
Nach Abschluss der Installation erscheint wieder der Cursor im Terminal.

Tippen Sie ins Terminal ```cd /media``` gefolgt von Ihrem Benutzernamen und ```/VBox``` ein. Drücken Sie anschließend die Tabulator-Taste. Dadurch wird der Name der zuvor eingehängten CD ergänzt. Ihre Eingabe muss so ähnlich wie in der folgenden Abbildung aussehen:

![](media/02/image29.png)

Drücken Sie nun die Enter-Taste.
Mit dem soeben ausgeführten Befehl wechseln Sie innerhalb des Terminal auf die eingehängte CD. (Das „cd“ im Befehl steht für „change directory“.)

Geben Sie diesen Befehl ein und führen Sie diesen aus:

```bash
sudo ./VBoxLinuxAdditions.run
```

Damit installieren Sie die Gasterweiterungen. Nach Abschluss der Installation erscheint wieder der Cursor. Tippen Sie exit ein, um das
Terminal zu schließen.

Öffnen Sie das Startmenü und klicken Sie in der rechten unteren Ecke den Ausschaltknopf an.

![](media/02/image30.png)

Es öffnet sich ein Fenster:

![](media/02/image31.png)

Klicken Sie „Herunterfahren“ an.

## Entfernen des Mediums mit den Gasterweiterungen (Nicht mehr nötig)

Wählen Sie in der Liste der Virtuellen Maschinen in VirtualBox den
Eintrag Ihrer Virtuellen Maschine aus.

Klicken Sie das gelbe Zahnrad für „Ändern“ an:

![](media/02/image32.png)

Es öffnen sich die Einstellungen:

![](media/02/image33.png)

Wechseln Sie in den Menüpunkt „Massenspeicher“ und wählen Sie dort unter „Controller: IDE" den Eintrag „VBoxGuestAdditions.iso“ aus:

![](media/02/image34.png)

Klicken Sie auf das CD-Symbol rechts und wählen Sie in dem sich öffnenden Menü "Entfernt das virtuelle Medium aus dem Laufwerk" aus:

![](media/02/image35.png)

## Aktivieren der gemeinsamen Zwischenablage

Wechseln Sie in den Menüpunkt „Allgemein“ und wählen Sie den Reiter „Erweitert“ aus. Stellen Sie dort die „Gemeinsame Zwischenablage“ und "Drag'n'Drop" auf „bidirektional“ ein:

![](media/02/image36.png)

Anschließend können Sie Texte auf dem Host in die Zwischenablage kopieren und in Xubuntu verwenden oder umgekehrt.

Klicken Sie auf "Ok".

## Gemeinsamer Ordner

Wechseln Sie in den Menüpunkt „Allgemein“ und wählen Sie einen "Gemeinsamer Ordner" aus. 

Somit haben Sie in der VM direkten Zugriff auf die Dateien aus dem Windows Host.

Wählein Sie dazu die Einstellungen wie im Bild angegeben.


![](media/02/image42.png)

Sie können also immer auch direkt auf Dateien in Windows zugreifen sollte es Probleme beim Download geben.

Klicken Sie auf "Ok".

## Setzen eines Sicherungspunktes

In VirtualBox können Sie sogenannte Sicherungspunkte setzen. Ein Sicherungspunkt speichert den aktuellen Zustand einer Virtuellen Maschine. Dadurch ist es zu einem späteren Zeitpunkt möglich, zu einem vorhandenen Sicherungspunkt zurückzukehren und die Virtuelle Maschine in einem früheren Zustand nutzen zu können.

Klicken Sie in VirtualBox auf das Toast-Menü Ihrer virtuellen Maschine und wählen dann „Sicherungspunkte“ aus:

![](media/02/image37.png)

Es öffnet sich die Liste der vorhandenen Sicherungspunkte. Da Sie bisher keine Sicherungspunkte gesetzt haben, ist diese leer.

Der Zustand Ihrer Virtuellen Maschine, welcher über „Starten“ angesprochen wird, ist in der Liste der Sicherungspunkte immer mit „Aktueller Zustand“ beschrieben:

![](media/02/image38.png)

Klicken Sie das Symbol mit der Kamera (Erzeugen) an.

Es öffnet sich ein neues Fenster. Geben Sie einen Namen und eine Beschreibung für den Sicherungspunkt ein:

![](media/02/image39.png)

Klicken Sie anschließend „OK“.

Die Liste der Sicherungspunkte sieht dann so aus:

![](media/02/image40.png)

Am besten Sie gewöhnen sich an immer wieder Sicherungen zu machen, somit könenn Sie jederzeit zu einer vorherigen laufenden Version zurückkehren wenn mal was schief geht im Laufe dieses Tutorials... :grinning:

## Dummy VirtualBox Datei importieren

Sollten Sie Probleme mit der Installation von Xubuntu haben können Sie auch versuchen eine vorbereitete Datei für VirtualBox zu importieren.

Im Verzeichnis /media liegt die Datei VuFind01Dummy.ova, diese können Sie direkt in Virtual Box importieren.
