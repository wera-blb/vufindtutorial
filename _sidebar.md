
- C:
  - [01 Installation VirtualBox](C:\xampp\htdocs\vufindtutorialde\01_Installation_VirtualBox.md)
  - [02 Installation Xubuntu](C:\xampp\htdocs\vufindtutorialde\02_Installation_Xubuntu.md)
  - [03 Installation VuFind](C:\xampp\htdocs\vufindtutorialde\03_Installation_VuFind.md)
  - [04 Installation Testimport](C:\xampp\htdocs\vufindtutorialde\04_Installation_Testimport.md)
  - [05 Konfiguration Ueberblick](C:\xampp\htdocs\vufindtutorialde\05_Konfiguration_Ueberblick.md)
  - [06 Konfiguration Allgemein](C:\xampp\htdocs\vufindtutorialde\06_Konfiguration_Allgemein.md)
  - [07 Konfiguration Katalogoberflaeche](C:\xampp\htdocs\vufindtutorialde\07_Konfiguration_Katalogoberflaeche.md)
  - [08 Konfiguration Facetten](C:\xampp\htdocs\vufindtutorialde\08_Konfiguration_Facetten.md)
  - [09 Konfiguration Kataloganreicherungen](C:\xampp\htdocs\vufindtutorialde\09_Konfiguration_Kataloganreicherungen.md)
  - [10 Konfiguration Suche Teil I](C:\xampp\htdocs\vufindtutorialde\10_Konfiguration_Suche_Teil_I.md)
  - [11 Datenimport MARC21 MARCXML](C:\xampp\htdocs\vufindtutorialde\11_Datenimport_MARC21_MARCXML.md)
  - [12 Konfiguration Suche Teil II](C:\xampp\htdocs\vufindtutorialde\12_Konfiguration_Suche_Teil_II.md)
  - [README](C:\xampp\htdocs\vufindtutorialde\README.md)
  - [SUMMARY](C:\xampp\htdocs\vufindtutorialde\SUMMARY.md)
