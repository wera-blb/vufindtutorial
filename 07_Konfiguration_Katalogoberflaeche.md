# Anpassung der Katalogoberfläche

Die Gestaltung der Katalogoberfläche basiert auf sogenannten Templates. Es gibt HTML-Templates für den Aufbau einer Seite und CSS-Templates für das Layout. Die HTML-Templates enthalten außerdem Programmcode in der Skriptsprache PHP, in welcher die Webseiten von VuFind programmiert sind. Alle Templates zusammen werden als Theme bezeichnet.

In diesem Kapitel werden wir ein eigenes Theme anlegen, den Footer von VuFind anpassen und das VuFind-Logo gegen ein eigenes Logo austauschen. Diese Schritte sollen Ihnen exemplarisch ein Grundverständnis für die Anpassung von Themes in VuFind geben.

## Eigenes Theme erstellen

Mit Version 4.1 bietet VuFind ein Kommandozeilen-Tool, um ein neues Theme zu erstellen. Das letzte Wort im folgenden Befehl definiert den Namen für das neue Theme (hier werden wir `myTheme` verwenden).

```bash
php /usr/local/vufind/public/index.php generate theme myTheme
```
![](media/07/image0.png)

Die Dateien für das neue Theme liegen anschließend im Ordner `/usr/local/vufind/themes/myTheme`. Die lokale Konfigurationsdatei `/usr/local/vufind/local/config/vufind/config.ini` wurde automatisch angepasst. Sie können nun über die Webseite in einem neuen Menüpunkt `Layout` zwischen dem Standard-Theme (Bootstrap) und dem neuen Theme umschalten.
Aktivieren Sie das neue Theme.

![](media/07/image1.png)

## Anpassung des Footers

Kopieren Sie mit dem folgenden Befehl die Template-Datei `footer.phtml` aus dem Standard-Theme (`/usr/local/vufind/themes/bootstrap3/templates/`) in das Verzeichnis `templates` Ihres Themes.

```bash
cp /usr/local/vufind/themes/bootstrap3/templates/footer.phtml /usr/local/vufind/themes/myTheme/templates/
```

Öffnen Sie anschließend die Datei mit einem Text Editor, und kommentieren Sie bspw. die folgenden Zeilen aus und speichern Sie die Datei.

Kommentare in HTML starten mit `('<!--')` und enden mit `('-->')`



```html
<!--
      <h2><?=$this->transEsc('Need Help?')?></h2>
        <ul>
          <li><a href="<?=$this->url('help-home')?>?topic=search&amp;_=<?=time() ?>" data-lightbox class="help-link"><?=$this->transEsc('Search Tips')?></a></li>
          <li><a href="<?=$this->url('content-page', ['page' => 'asklibrary']) ?>"><?=$this->transEsc('Ask a Librarian')?></a></li>
          <li><a href="<?=$this->url('content-page', ['page' => 'faq']) ?>"><?=$this->transEsc('FAQs')?></a></li>
        </ul>`
-->
```
Durch die Kommentieurng wird der Hilfe Bereich nicht mehr angezeigt

Die Änderung wird sofort auf der Webseite sichtbar:

![](media/07/image2.png)

## Änderung des Logos über dem Suchschlitz

Legen Sie im Verzeichnis `/usr/local/vufind/themes/myTheme` das gewünschte Logo ab. Wenn Sie kein eigenes Logo parat haben, können Sie das hier verwendete  `myImage.png` herunterladen:

```bash
wget https://www.blb-karlsruhe.de/typo3conf/ext/blbweb/Resources/Public/Images/bg.jpg -O /usr/local/vufind/themes/myTheme/images/myImageBg.png
```

```bash
wget https://www.blb-karlsruhe.de/files/user_upload/Logo/blb.svg
 -O /usr/local/vufind/themes/myTheme/images/myImageLogo.svg
```


Stellen Sie mit folgendem Befehl sicher, dass das Logo vom Webserver gelesen werden kann:

```bash
chmod +r /usr/local/vufind/themes/myTheme/images/myImageBg.png
```
und
```bash
chmod +r /usr/local/vufind/themes/myTheme/images/myImageLogo.svg
```

Öffnen Sie nun im Verzeichnis `/usr/local/vufind/themes/myTheme/less` die Datei `custom.less` mit einem Text Editor.

1. Ersetzen Sie `@image-path: "../../local_theme_example/images";` durch `@image-path: "../../images";`
2. Ersetzen Sie den Abschnitt `header` wie folgt:

```css
header {
  .navbar {
    .navbar-brand {
      background-image: url('../../images/myImageLogo.svg');
      height: 100px;
      width: 150px;
      background-repeat: no-repeat;
      background-position: center;
      color:transparent;
    }
  }
}
```

Unter `background-image` tragen Sie den Pfad Ihres Bildes ein.

Führen Sie abschließend den folgenden Befehl im Terminal aus, um die veränderte Konfiguration zu aktivieren:

## NodeJS & NPM installieren

s.a.: [NodeJS installieren](https://vufind.org/wiki/development:npm)

Als root user

```bash
apt-get install nodejs
```

Korrekte Installation überprüfen
```bash
nodejs -v
```
und
```bash
npm -v
```
sollten Ihnen die installierte Version anzeigen.

Im Verzeichnis ```/usr/local/vufind``` alle Abhängigkeiten installieren
```bash
npm install
```

* Grunt installieren

```bash
npm install grunt-cli --location=global 
```

```bash
npm run build:less
```

```bash
php /usr/local/vufind/util/cssBuilder.php
```

Der Befehl baut aus den Dateien im Verzeichnis `less` jedes(!) Themes eine neue Datei namens `compiled.css` im Unterverzeichnis `css` zusammen.

Wenn Sie anschließend in Firefox die VuFind-Startseite neu aufrufen, wird oben links das verwendete Logo angezeigt.

![](media/07/image5.png)


Um das Hintergrundbild zu ändern öffen Sie nun im Verzeichnis `/usr/local/vufind/themes/myTheme/less` die Datei `home-page.less` mit einem Text Editor.

2. Ersetzen Sie im Abschnitt `.search-hero` den Bildpfad wie folgt:

```css
  color: #000;
  background-image: url('@{image-path}/myImageBg.png');
```
und setzen Sie die Farbe für die Links auf schwarz:
```css
  a { color: #000; }
  ```

Den Eintrag  `.image-credit` blenden wir noch aus:
```css
.image-credit {
  display:none;
}
  ```

Danach wieder
```bash
php /usr/local/vufind/util/cssBuilder.php
```
ausführen und die Seite neu laden.

Sie können nun das neue Hintergrundbild sehen, der Bildnachweis wird nicht mehr angezeigt.

![](media/07/image6.png)

## Sicherungspunkt in VirtualBox setzen

Und zur Sicherheit nun wieder einen Sicherungspunkt anlegen.

Fahren Sie das Betriebssystem herunter und setzen Sie in VirtualBox einen weiteren Sicherungspunkt namens „mit Testimport“.

## Quellen

VuFind Dokumentation: Code Generatoren
<https://vufind.org/wiki/development:code_generators#creating_themes>

VuFind Dokumentation: User Interface Customization
<https://vufind.org/wiki/development:architecture:user_interface>

VuFind Dokumentation: LESS / SASS
<https://vufind.org/wiki/development:architecture:less>
