# Installation von VuFind

In den vorigen beiden Kapiteln haben Sie ein Ubuntu-Linux-Betriebssystem in VirtualBox installiert. In diesem Kapitel installieren wir nun VuFind und die dafür benötigten Softwarekomponenten. Dazu gehören:

* Webserver (Apache)
* Suchindex (Solr)
* Programmiersprache (PHP)
* Datenbank (MySQL)

## Aktualisierung des Betriebssystems

Vor der Installation sollte das Betriebssystem auf den aktuellsten Stand gebracht werden. Führen Sie dazu folgenden Befehl im Terminal aus:

```bash
sudo apt-get update && sudo apt-get dist-upgrade
```

Der Installation der Updates müssen Sie mit der Antwort „J“ auf die Frage „Möchten Sie fortfahren? \[J/n\]“ zustimmen.

Anschließend sollte das System neu gestartet werden:

```bash
sudo shutdown -r now
```

## Download des Installationspaketes

Sehen Sie auf der Webseite von vufind unter https://vufind.org/wiki/installation:ubuntu nach welches die aktuelle Version für vufiind ist, zu finden dort unter dem Punkt ***1. Obtain the package***

![](media/03/image1.png)

Kopieren Sie den Befehl und führen Sie diesen im Terminal aus.

```bash
wget https://github.com/vufind-org/vufind/releases/download/v9.1/vufind_9.1.deb
```

Sie werden einen Feher bekommen, denn auch für wget müssen wir Proxy Einstellungen vornehmen.

Öffnen Sie als root die Datei

```bash
nano /etc/wgetrc
```

und suchen nach dem Begriff proxy.

Haben Sie die Zeilen in ähnlicher Form wie diese gefunden

```bash
https_proxy = http://172.20.10.2:8080
http_proxy = http://172.20.10.2:8080
ftp_proxy = http://172.20.10.2:8080
```
so entfernen Sie jeweils das # Zeichen und ersetzen den Wert mit unserem Proxy wie hier angegeben.

Dann funktioniert der Download :-)


Die Installationsdatei (ein sogenanntes Paket) wird in Ihr Homeverzeichnis heruntergeladen:

![](media/03/image2.png)

## Installation des Paketes

Führen Sie im Terminal folgenden Befehl aus:

```bash
sudo dpkg -i vufind_x.x.x.deb
```

Sollte es zu Fehlermeldungen kommen, bitte folgenden Befehl ausführen
```bash
sudo apt install -f ~/vufind_x.x.x.deb
```
***Der Name der Datei bzgl Version von vufind ist entsprechend anzupassen.***

Dieser Befehl installiert VuFind und alle für VuFind benötigten Pakete (sogenannte abhängige Pakete). Bei diesen Paketen handelt es sich um die in der Einleitung beschriebenen Softwarekomponenten (Apache, Solr, MySQL, PHP).

Der Installation müssen Sie mit der Antwort „J“ auf die Frage „Möchten Sie fortfahren? \[J/n\]“ zustimmen.

Jetzt werden viele Pakete heruntergeladen, danach installiert.
Kann alles wieder eine Weile dauern... :grinning:

![](media/03/image3.png)

Wenn die Installation abgeschlossen ist, starten Sie den Firefox Browser über den Button Internetnavigation. Rufen Sie die Webseite <http://localhost/vufind> auf. Nach kurzer Zeit erscheint die Startseite von VuFind:

![](media/03/image6.png)

Der Hinweis „Es ist ein Fehler aufgetreten“ erscheint, weil die Suchmaschine Solr noch nicht gestartet wurde.


----

Falls für die Installation von MySQL die Angabe eines Root-Kennwortes ***NICHT*** erfragt wird:

![](media/03/image4.png)

* Geben Sie im Terminal folgenden Befehl ein:
```bash
sudo mysql -uroot
```
und dann
```bash
UPDATE mysql.user SET plugin='mysql_native_password' WHERE User='root'; FLUSH PRIVILEGES;"
```
* Verlassen Sie MySQL mit dem Befehl
```bash
quit;
```

und geben Sie danach
```bash
sudo /usr/bin/mysql_secure_installation
```
im  Terminal ein um ein neues Passwort zu setzen.

Beantworten  Sie alle Fragen mit "Y".

Notieren Sie sich das Kennwort, da es für die spätere Konfiguration von VuFind benötigt wird.


## Berechtigungen setzen und Solr starten

**Solr muss nach jedem Hochfahren der VM neu gestartet werden.**

Ein automatischer Start ist natürlich auch möglich aber nicht Teil dieses Tutorials

Führen Sie im Terminal folgenden Befehl aus:

```bash
source /etc/profile
```

Durch diesen Befehl werden die Umgebungsvariablen neu eingelesen, was uns einen Neustart erspart. Die Umgebungsvariablen werden zum Betrieb von VuFind benötigt und enthalten den Pfad zur Java-Instanz, zum Hauptverzeichnis von VuFind sowie zum Verzeichnis mit Ihren lokalen Einstellungen für VuFind.

Geben Sie folgende Befehle ein, um notwendige Berechtigungen für Solr und VuFind zu konfigurieren:

```bash
sudo chown -R $(id -u):$(id -g) /usr/local/vufind/
sudo chown -R www-data:www-data /usr/local/vufind/local/config
sudo chown -R www-data:www-data /usr/local/vufind/local/cache
```

Der erste Befehl setzt den aktuell angemeldeten Nutzer als Eigentümer des Verzeichnisses `/usr/local/vufind`. Damit der Apache-Webserver auf bestimmte Verzeichnisse zugreifen kann, wird für die Unterordner `local/config` und `local/cache` der Nutzer `www-data` als Eigentümer gesetzt.

Starten Sie dann den Suchindex Solr:

```bash
/usr/local/vufind/solr.sh start
```
Die Meldungen im Terminal können Sie erstmal ignorieren.

Wichti ist das am Ende der Text
```bash
Started Solr server on port 8983 (pid=18042). Happy searching!
```
steht.

Wenn Sie die noch im Firefox geöffnete Startseite von VuFind aktualisieren, wird der Fehlerhinweis nicht mehr angezeigt:

![](media/03/image7.png)

## Automatische Konfiguration

Öffnen Sie jetzt die Seite <http://localhost/vufind/Install/Home>. Angezeigt wird die Autokonfiguration von VuFind:

![](media/03/image8.png)

### Grundkonfiguration

Klicken Sie den Schalter „Reparieren“ hinter „Basic Configuration“ an. Dies wird quittiert mit „Your configuration has been successfully updated.“ Wechseln Sie über den Link "Autokonfiguration" wieder zurück auf die vorige Seite.

### Datenbank

Beim Reparieren von „Database“ erscheint eine Eingabemaske:

![](media/03/image9.png)

Geben Sie in das Feld „New user password“ ein Kennwort ein. Wiederholen Sie dieses Kennwort im Feld „Confirm new user password“. Notieren Sie sich das Kennwort.

Geben Sie im Feld "Mysql Root Password" das Passwort für den Nutzer root ein das Sie im vorherigen Schritt vergeben haben.

Klicken  Sie "Abschicken".

## Anlegen eines Nutzers für phpMyAdmin

Wir wollen später die Software "phpMyAdmin" installieren, dazu müssen wir zuerst noch den user phpmyadmin in MySQL anlegen.

Öffnen Sie dazu ein Terminal und geben folgenden Befehl ein um sich mit der Datenbank mysql zu verbinden.
Sie müssen dazu das Passwort für den root user das Sie oben verwendent haben eingeben.
```bash
mysql –u root –p
```
Nach erfolgreicher Anmeldung haben Sie sich mit der Datenbank verbunden, erkennbar an ***mysql>***.

Geben Sie nun folgenden Befehl ein, wobei Sie ein beliebiges Passwort wählen können, 'PHP_Passwort123$' ist nur ein Bsp.

Zum allgemeinen Verständnis, welche Anforderungen das Passwort erfüllen muss s.a.
<https://ostechnix.com/fix-mysql-error-1819-hy000-your-password-does-not-satisfy-the-current-policy-requirements/>

```bash
mysql> create user 'phpmyadmin'@'localhost' identified by 'PHP_Passwort123$';
```


Nun installieren wir uns die Software "phpmyadmin" um das SQL auszuführen. Im Terminal folgenden Befehl eingeben:
```bash
sudo apt-get install phpmyadmin
```
![](media/03/image13.png)

Die Abfrage wieder mit "J" bestätigen.

In der Frage nach dem Webserver welcher konfiguriert werden soll, wählen Sie mit der Leertaste "apache2" aus

![](media/03/image14.png)

Mit der Tabulator Taste sprichen Sie auf "ok", drücken Sie dann die Enter Taste.

Bei der Frage nach der phpmyadmin Konfiguration mit dbconfig-common wählen Sie "Ja" durch Drücken der Enter Taste.

![](media/03/image15.png)

Geben Sie ein Passwort für den Nutzer phpmyadmin ein:

![](media/03/image16.png)

Und bestätigen Sie dieses im folgenden Fenster.

***Und bitte merken!***

Rufen Sie nun in Firefox die Seite http://localhost/phpmyadmin/ auf und loggen Sie sich mit dem user "phpmyadmin" und dessen Passwort ein.

![](media/03/image9.png)

## Anbindung Bibliothekssystem

Beim Reparieren von „ILS“ erscheint ebenfalls eine Eingabemaske:

![](media/03/image10_.png)

Wählen Sie „NoILS“ aus der Liste aus. Klicken Sie anschließend „Daten absenden“ an.

---------

**!!!** Alternativ können Sie Ihr Lokalsystem aus der Liste wählen und es konfigurieren. Beachten Sie jedoch, dass dies nicht Bestandteil dieses Tutorials ist.

---------

Zurück auf der Konfigurationsseite erhalten wir weiterhin die Fehlermeldung "ILS... Fehlgeschlagen". Die Autokonfiguration hat die Einstellung auf NoILS mit der Option „ils-offline“ gesetzt, die für Wartungsarbeiten gedacht ist. Wir müssen diese noch auf „ils-none“ setzen, um VuFind zu signalisieren, dass tatsächlich kein Lokalsystem angebunden ist. Diese Einstellung ist in der Datei `/usr/local/vufind/local/config/vufind/NoILS.ini` vorzunehmen. Geben Sie dazu im Terminal folgenden Befehl ein:

```bash
sudo sed -i 's/mode = ils-offline/mode = ils-none/g' /usr/local/vufind/local/config/vufind/NoILS.ini
```

Wenn Sie anschließend die Seite Autokonfiguration neu laden, wird "ILS... OK" angezeigt.

### Sicherheitseinstellungen

Klicken Sie abschließend den Schalter „Reparieren“ im Bereich „Security“ an.

Unterhalb der Statusmeldungen erscheint nun ein Link zum Abschalten der Autokonfiguration:

![](media/03/image11_.png)

**Sollte sich SSL nicht reparieren lassen ist die im Zuge dieses Tutorials nicht weiter wichtig und zu ignorieren.**

Klicken Sie „Disable Auto Configuration“ an (wenn verfügbar, sonst ignorieren und weiter machen).

Dies wird mit „Auto configuration has been successfully disabled.“ und der Empfehlung quittiert, die Berechtigungen für das Konfigurationsverzeichnis sicherheitshalber anzupassen. Geben Sie dazu noch den folgenden Befehl ins Terminal ein:

```bash
sudo chown -R $(id -u):$(id -g) /usr/local/vufind/local/config
```

Dieser Befehl sorgt dafür, dass Ihr Benutzer zum Besitzer des Verzeichnisses `/usr/local/vufind/local/config` wird und dieses nicht mehr durch den Webserver verändert werden kann. Das Verzeichnis enthält die lokale Konfiguration für VuFind (mehr dazu in den folgenden Kapiteln).

## Zwischenstand

Die automatische Konfiguration von VuFind ist damit abgeschlossen und das System einsatzbereit.

Im nächsten Schritt importieren wir Testdaten, denn momentan ist der Suchindex noch komplett leer.

## Sicherungspunkt in VirtualBox setzen

Fahren Sie nun das Betriebssystem herunter und setzen Sie in VirtualBox einen Sicherungspunkt namens „VuFind, bereit für Testimport“.

## Quellen (Stand: 13.06.2022)

VuFind Dokumentation: Datenbank Issues
<https://vufind.org/wiki/installation:ubuntu#database_issues>

VuFind Dokumentation: Installation Notes
<https://vufind.org/wiki/installation:ubuntu>
