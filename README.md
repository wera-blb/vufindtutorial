# Tutorial: Installation und Konfiguration einer Demo von VuFind mit VirtualBox und Xubuntu

* In dem Tutorial wird das Linux Derivat Xubuntu genutzt
* Es wird vufind als Paket installiert, inkl. aller benötigten Abhängigkeiten.

## History

**28.09.2023**
* Update der Anleitung für 22.04.3-LTS und vufind 9

**13.01.2023**
* Anleitung um Konfiguration für Proxy-Einstellungen zur Nutzung innerhalb der BLB erstellt
* Ergänzung um Erstellung eines Nutzers über mysql für phpmyadmin
* Ergänzung um evlt. Überspringen der SSL Reperatur bei Fehler

**23.06.2022**
* Dummy VuFin01Dummy.ova Datei für VirtualBox erstellt
* Xubuntu 22.04-LTS installiert
* Updates installiert
* Fehlende Sprachunterstützung installiert
* Gasterweiterungen installiert
* Bidirektionale Drag'n'Drop eingerichtet

**02.06.2022**
* Update auf Xubuntu 22.04-LTS
* Anleitung auf aktuelle vufind Version 8.0.4 angepasst

**12.02.2017**
* Initial Version mit Ubuntu Mate 2016 basierend auf einer [Projektarbeit von Stefan Niesner](http://malisprojekte.web.th-koeln.de/wordpress/stefan-niesner/) aus dem Jahr 2015 und wird laufend für neue VuFind-Versionen aktualisiert.<br>
[Quellcode bei GitHub (Markdown)](https://github.com/felixlohmeier/vufindtutorialde)

## Inhalte

01. [Installation von VirtualBox](01_Installation_VirtualBox.md)
02. [Installation von Linux (Xubuntu)](02_Installation_Xubuntu.md)
03. [Installation von VuFind](03_Installation_VuFind.md)
04. [Durchführung eines Testimports](04_Installation_Testimport.md)
05. [Überblick zur Konfiguration](05_Konfiguration_Ueberblick.md)
06. [Allgemeine Einstellungen](06_Konfiguration_Allgemein.md)
07. [Anpassung der Katalogoberfläche](07_Konfiguration_Katalogoberflaeche.md)
08. [Konfiguration der Facetten](08_Konfiguration_Facetten.md)
09. [Konfiguration von Kataloganreicherungen](09_Konfiguration_Kataloganreicherungen.md)
10. [Konfiguration der Suche, Teil I](10_Konfiguration_Suche_Teil_I.md)
11. [Datenimport MARC21 MARCXML](11_Datenimport_MARC21_MARCXML.md)
12. [Konfiguration der Suche, Teil II](12_Konfiguration_Suche_Teil_II.md)

## Systemvoraussetzungen

* Computer mit Windows, Mac oder Linux
* Administrationsrechte zur Installation von Software
* 4 GB freien Arbeitsspeicher für die virtuelle Maschine
* mind. 25 GB freien Speicher auf der Festplatte

## Dokumentation
* Zur Erstellung der Seiten dient [docsify]([https://](https://docsify.js.org))
* Installation über
```bash
npm i docsify-cli -g
```
* Erstellung der Seiten im gleichen Verzeichnis mit dem Befehl
```bash
docsify generate .
```


## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Namensnennung-Nicht kommerziell 4.0 International Lizenz</a>.
