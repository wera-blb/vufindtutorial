# Konfiguration der Suche, Teil II

Dieser Teil der Suche setzt voraus, dass Sie den Datenimport im vorherigen Kapitel (Daten der BLB) durchgeführt haben.

## Synonyme

Suchen Sie in Vufind nach dem Suchterm "Amerika":

![](media/12/image1.png)

Suchen Sie anschließend nach dem Suchterm "USA":

![](media/12/image2.png)

Gehen Sie wie im Folgenden beschrieben vor, um die Suchterme "Amerika" und "USA" bei der als Synonyme zu definieren.

Öffnen Sie im Verzeichnis `/usr/local/vufind/solr/vufind/biblio/conf/` die Datei `synonyms.txt` mit Mousepad.

Fügen Sie in die Datei folgende Zeile ein

```
USA,usa,Amerika,amerika,AMERIKA
```

Speichern Sie die Datei und starten Sie VuFind neu.
```bash
/usr/local/vufind/solr.sh restart
```

Führen Sie die beiden Suchen nach "Amerika" und "USA" erneut durch:

![](media/12/image3.png)

![](media/12/image4.png)

Für beide Suchterme ergeben sich nun die gleiche Anzahl an Treffer.

## Standard Query Parser Parameters

Mit den Standard Query Parser Parameters können Sie die Suchsyntax beeinflussen. Standardmäßig werden Suchbegrifft mit "AND" verknüpft.

Führen Sie in VuFind eine Suche nach "auto beruf motorrad" durch. Diese ergibt keine Ergebnisse:

![](media/12/image5.png)

Öffnen Sie im Verzeichnis `/usr/local/vufind/solr/vufind/biblio/conf/` die Datei `solrconfig.xml` mit Mousepad.

Suchen Sie darin diese Zeile:

```
<requestHandler name="dismax" class="solr.SearchHandler">
```

Ersetzen Sie in der Angabe `<str name="q.op">AND</str>` durch `<str name="q.op">OR</str>`

Speichern Sie die Datei und starten Sie VuFind neu.
```bash
/usr/local/vufind/solr.sh restart
```

Führen Sie die Suche nach "auto beruf motorrad" nun erneut durch.

Die gleiche Suche liefert nun 5 Treffer, da die einzelnen Suchbegriffe statt mit "AND" mit "OR" verknüpft werden, d.h. nur noch einer der Begriffe muss in einem Treffer vorkommen, nicht mehr alle.

![](media/12/image6.png)

## Reduktion von Wortformen (Stemming)

In der Standard-Konfiguration von VuFind ist der für die englische Sprache optimierte Porter-Stemming-Algorithmus in der Datei
```
/usr/local/vufind/solr/vufind/biblio/conf/schema.xml
```
vorkonfiguriert
```
<filter class="solr.SnowballPorterFilterFactory" language="English"
protected="protwords.txt"/>
```

Was genau bedeutet nun Stemming?

Ein Beispiel mit ausgedachten Werten, Sie werden bei Ihrer Suche andere Zahlen angezeigt bekommen, daber das Prinzip wird verständlich.

Eine Suche nach `Haus` hat 6 Treffer.

![](media/12/image7.png)

Eine Suche nach `Häuser` hat 2 Treffer.

![](media/12/image8.png)

Stellen wir den Wert im String
```
<filter class="solr.SnowballPorterFilterFactory" language="English"
protected="protwords.txt"/>
```

nun auf Deutsch um
```
<filter class="solr.SnowballPorterFilterFactory" language="German"
protected="protwords.txt"/>
```
so findet man mit der Suche nach `Haus` weiterhin 6 Treffer, mit der Suchen nach `Häuser`  nun allerdings **8 Treffer!**

![](media/12/image9.png)

Der Bergriff `Häuser` wird also zusätzlich noch auf seine Wortform `Haus` reduziert und es wird mit diesen beiden Begriffen gesucht.
Daher bekommt man nun also alle Treffer 8 Treffer angezeigt.

## Quellen

Stop Words and Synonyms. VuFind Documentation.
<https://vufotoind.org/wiki/stop_ words_and_synonyms>

Solr: Standard Query Parser Parameters
<https://solr.apache.org/guide/6_6/the-standard-query-parser.html>

Solr: Snowball Porter Stemmer Filter
<https://solr.apache.org/guide/6_6/filter-descriptions.html#FilterDescriptions-SnowballPorterStemmerFilter>
